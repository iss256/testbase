#include <iostream>
#include <list>
#include "include/line2d.h"
#include "include/start2d.h"

using namespace std;

int main(int argc, char *argv[])
{
    list<BaseElement*> contour;

    contour.push_back(new Start2d(0,0));
    contour.push_back(new Line2d(0,100));
    contour.push_back(new Line2d(100,100));
    contour.push_back(new Line2d(100,0));
    contour.push_back(new Line2d(0,0));

    for(BaseElement* &element: contour)
    {
        cout<<element->toString()<<endl;
        delete element;
    }

    return 0;
}

