#include "include/line2d.h"


Line2d::Line2d(double x, double y)
{
    m_X = x;
    m_Y = y;
    type = line2d;
}

Line2d::~Line2d()
{

}

std::string Line2d::toString()
{
    return "Line2d: x = " + to_string(m_X) + ", y = " + to_string(m_Y);
}

